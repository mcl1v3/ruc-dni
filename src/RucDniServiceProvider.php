<?php

namespace Mcl1v3\RucDni;

use Illuminate\Support\ServiceProvider;

class RucDniServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //$this->loadRoutesFrom(__DIR__.'/routes.php');
    }

    /** php artisan vendor:publish --tag=mcl1v3\ruc-dni\RucDniServiceProvider  

     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Mcl1v3\RucDni\ConsultasController');
    }
}
