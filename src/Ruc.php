<?php

namespace Mcl1v3\RucDni;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use Mcl1v3\RucDni\HtmlDomParser;

class Ruc extends Model
{
    //
    public static function get($ruc){        
        if(strlen($ruc)==11){
            $return=[];
            $client = new Client(['cookies' => true]);
            $resprnd = $client->request('GET', 'http://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/captcha?accion=random');
            $rnd=(string)$resprnd->getBody();
            $response = $client->request('GET', 'http://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias?accion=consPorRuc&nroRuc='.$ruc.'&numRnd='.$rnd.'&tipdoc=');
            $dom = HtmlDomParser::str_get_html( $response->getBody() );
            $table = $dom->find('table')[0];
            $elems=$table->find('tr');
            if(substr($ruc,0,2)=="20"){
                $rz=self::getTD($elems[0],1);                
                
                $return['ruc']=substr($rz ,0, strpos($rz, "-")-1);
                $return['razonSocial']=substr($rz , strpos($rz, "-")+2);
                $return['nombreComercial']=self::getTD($elems[2],1);
                $return['telefonos']=self::getTD($elems[2],1);
                $return['tipo']=self::getTD($elems[1],1);
                $return['estado']=self::getTD($elems[4],1);
                $return['condicion']=self::getTD($elems[5],1);
                $dir=self::getTD($elems[6],1);

                $tdir=substr($dir , 0,strrpos($dir, "-")-2);
                $tdir=substr($tdir , 0,strrpos($tdir, "-")-2);
                $tdir=substr($tdir , 0,strrpos($tdir, " "));

                $return['direccion']=$tdir;

                $dep=substr($dir ,0, strrpos($dir, "-")-1);
                $tdir=substr($dep , 0,strrpos($dep, "-")-1);
                $dep=substr($dep ,strrpos($dep, " ")+1);

                $return['departamento']=$dep;

                $prov=substr($dir ,0, strrpos($dir, "-")-1);
                $prov=substr($prov ,strrpos($prov, "-")+2);

                $return['provincia']=$prov;

                $dis=substr($dir ,strrpos($dir, "-")+2);

                $return['distrito']=$dis;
                $return['fechaInscripcion']=self::getTD($elems[3],1);
                $return['sistEmsion']=self::getTD($elems[7],1);
                $return['actExterior']=self::getTD($elems[7],3);
                $return['sistContabilidad']=self::getTD($elems[8],1);        
                $return['actEconomicas']=self::getTDSelect($elems[9],1);
                $return['cpPago']=self::getTDSelect($elems[10],1);
                $return['sistElectronica']=self::getTDSelect($elems[11],1);
                $return['emisorElectronico']=self::getTD($elems[12],1);
                $return['cpeElectronico']=self::getTD($elems[13],1);
                $return['fechaPle']=self::getTD($elems[14],1);
                $return['padrones']=self::getTDSelect($elems[15],1);
            }else{
                $ruc10=self::getTD($elems[0],1);
                $ruc10=substr($ruc10 ,0,strpos($ruc10, "-")-1);
                $return['ruc']=$ruc10;

                $nombres=self::getTD($elems[0],1);
                $nombres=substr($nombres ,strpos($nombres, "-")+2);

                $apellidos=substr($nombres ,0,strpos($nombres, " ",strpos($nombres, " ")+1));
                $nombres=substr($nombres ,strpos($nombres, " ",strpos($nombres, " ")+1)+1);
                $return['nombres']=$nombres;
                $return['apellidos']=$apellidos;

                $return['tipoContribuyente']=self::getTD($elems[1],1);

                $tdoc=self::getTD($elems[2],1); 
                $doc=substr($tdoc ,strpos($tdoc, " ")+1,(strpos($tdoc, "-")?strpos($tdoc, "-")-5:strlen($tdoc)));

                $tdoc=substr($tdoc ,0,strpos($tdoc, " "));
                $return['tipoDocumento']=$tdoc;
                $return['nroDocumento']=$doc;
                $return['nombreComercial']=self::getTD($elems[3],1);
                $return['fechaInscripcion']=self::getTD($elems[4],1);
                $return['fechaActividades']=self::getTD($elems[4],3);
                $return['estado']=self::getTD($elems[5],1);
                $return['condicion']=self::getTD($elems[6],1);
                $return['profesion']=self::getTD($elems[6],3);
                $return['direccion']=self::getTD($elems[7],1);
                $return['sistEmision']=self::getTD($elems[8],1);
                $return['actExterior']=self::getTD($elems[8],3);
                $return['sistContabilidad']=self::getTD($elems[9],1);
                $return['actEconomicas']=self::getTDSelect($elems[10],1);
                $return['cpPago']=self::getTDSelect($elems[11],1);
                $return['sistElectronico']=self::getTDSelect($elems[12],1);
                $return['emisorElectronico']=self::getTD($elems[13],1);
                $return['cpeElectronico']=self::getTD($elems[14],1);
                $return['fechaPle']=self::getTD($elems[15],1);
                $return['padrones']=self::getTDSelect($elems[16],1);
                
            }            
            return $return;
        }else{
            return ['error'=>'RUC INVALIDA'];
        }       
        
    }

    private static  function getTD($element,$index){
        $domt = HtmlDomParser::str_get_html( $element );
        $elemt=$domt->find('td');
        return self::clearFirstLastSpace(preg_replace('/\s+/', ' ',$elemt[$index]->plaintext));
    }

    private static  function getTDSelect($element,$index){
        $domt = HtmlDomParser::str_get_html( $element );
        $elemt=$domt->find('td')[$index];
        $elemt=$elemt->find('option');
        $array=[];
        foreach ($elemt as $key => $value) {
            $array[]=self::clearFirstLastSpace(preg_replace('/\s+/', ' ',$value->plaintext));
        }
        return $array;
    }
    private static  function clearFirstLastSpace($text){
        if(substr($text, 0,1)==" "){
            $text= substr($text ,1);
        }
        if(strrpos($text, " ")==(strlen($text)-1) && strrpos($text, " ")!=false ){
            $text=substr($text ,0,(strlen($text)-1));
            
        }
        return $text;
    }
}
