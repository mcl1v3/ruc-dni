<?php

namespace Mcl1v3\RucDni;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class Dni extends Model
{
    
    public static function get($dni){
        $dni=''.$dni;
        $client=new Client(['cookies' => true]);
        $person=$client->request('GET','http://aplicaciones007.jne.gob.pe/srop_publico/Consulta/Afiliado/GetNombresCiudadano?DNI='.$dni);
        $person=(string)$person->getBody();
        if(substr($person,0,3)!="|||"){
            $return=[];
            $return['dni']=$dni;
            $return['apePaterno']=substr($person ,0, strpos($person, "|"));

            $mat=substr($person ,strpos($person, "|")+1);
            $mat=substr($mat,0, strpos($mat, "|"));
            
            $return['apeMaterno']=$mat;
            $return['nombres']=substr($person ,strrpos($person, "|")+1);
            
            return $return;
        }else{
            return ['error'=>substr($person,3)];
        }
        
    }   
}
