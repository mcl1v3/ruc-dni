<?php

namespace Mcl1v3\RucDni;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mcl1v3\RucDni\Ruc;
use Mcl1v3\RucDni\Dni;

class ConsultasController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ruc($ruc){   
        $data=Ruc::get($ruc);  
        return response()->json($data);
    }
    public function dni($dni){   
        $data=Dni::get($dni);  
        return response()->json($data);
    }
    
}
