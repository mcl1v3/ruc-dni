## Consultas de RUC y DNI

Se podra realizar consultas de Ruc y Sunat, obteniente un arreglo mediante nuestras 2 clases.

## Intalacion

Usando composer desde packagist.org

```bash
composer require mcl1v3/ruc-dni
```

### Clases Disponibles
- Ruc — SUNAT.
- Dni — RENIEC.

### Uso de la clase Ruc

Debemos llamar al paquete
```bash
use Mcl1v3\RucDni\Ruc;
```

Usar la funcion de manera sencilla con unico parametro de ruc.
```bash
$datos=Ruc::get('20553797269');
```
obtendremos
```bash
{
"ruc": "20553797269",
"razonSocial": "GLOBAL INTERNATIONAL PERU S.A.C.",
"nombreComercial": "-",
"telefonos": "-",
"tipo": "SOCIEDAD ANONIMA CERRADA",
"estado": "ACTIVO",
"condicion": "HABIDO",
"direccion": "CAL.4 MZA. P LOTE. 19 URB. SAN ELIAS (CC EL PORVENIR DE SAN ELIAS)",
"departamento": "LIMA",
"provincia": "LIMA",
"distrito": "LOS OLIVOS",
"fechaInscripcion": "23/07/2013",
"sistEmsion": "MANUAL",
"actExterior": "SIN ACTIVIDAD",
"sistContabilidad": "MANUAL",
"actEconomicas": [
"52391 - OTROS TIPOS DE VENTA AL POR MENOR.",
"93098 - OTRAS ACTIVID.DE TIPO SERVICIO NCP"
],
"cpPago": [
"FACTURA",
"BOLETA DE VENTA",
"GUIA DE REMISION - REMITENTE"
],
"sistElectronica": [],
"emisorElectronico": "-",
"cpeElectronico": "-",
"fechaPle": "-",
"padrones": [
"NINGUNO"
]
}
```

### Uso de la clase Dni

Debemos llamar al paquete
```bash
use Mcl1v3\RucDni\Dni;
```

Usar la funcion de manera sencilla con unico parametro del dni.
```bash
$datos=Dni::get('72635041');
```
obtendremos
```bash
{
"dni": "72635041",
"apePaterno": "SALVATIERRA",
"apeMaterno": "ESPINOZA",
"nombres": "CESAR ANDRES"
}
``` 
### Ejemplo de Rutas
Tenemos un controlador que devuelve directamente los valore en formato JSON.
Tendriamos que poner en routes.
```bash
Route::get('consulta/ruc/{ruc}', '\Mcl1v3\RucDni\ConsultasController@ruc');
Route::get('consulta/dni/{dni}', '\Mcl1v3\RucDni\ConsultasController@dni');
```